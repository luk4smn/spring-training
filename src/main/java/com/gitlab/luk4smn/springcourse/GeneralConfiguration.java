package com.gitlab.luk4smn.springcourse;

import com.gitlab.luk4smn.springcourse.annotations.DevelopmentAnnotation;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;

@DevelopmentAnnotation
public class GeneralConfiguration {
    
    @Bean
    public CommandLineRunner executar(){
        return args -> {
            System.out.println("Subindo aplicação em modo de desenvolvimento");
        };
    }

}
