package com.gitlab.luk4smn.springcourse.services;

import java.util.List;

import com.gitlab.luk4smn.springcourse.models.Produto;
import com.gitlab.luk4smn.springcourse.repositories.ProdutoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ProdutoService {
    
    @Autowired
	private ProdutoRepository repository;

    public List<Produto> search(Produto filter){
		ExampleMatcher matcher = ExampleMatcher
		.matching()
		.withIgnoreCase()
		.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		Example example = Example.of(filter, matcher);

		return repository.findAll(example);
	}

	public Produto getById(Long id){
		return repository
		.findById(id)
		.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Transactional
	public Produto create(Produto produto){
		return repository.save(produto);
	}

	@Transactional
	public void update(Long id, Produto produto){
		repository
            .findById(id)
            .map(p -> {
				produto.setId(p.getId());
				return repository.save(produto);
			}).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Transactional
	public void delete(Long id){
		repository
			.findById(id)
			.map(produto -> {
				    repository.delete(produto);
					return produto;
				}
			).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}
}
