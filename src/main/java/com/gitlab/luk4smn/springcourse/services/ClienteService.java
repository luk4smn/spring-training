package com.gitlab.luk4smn.springcourse.services;

import java.util.List;

import com.gitlab.luk4smn.springcourse.models.Cliente;
import com.gitlab.luk4smn.springcourse.repositories.ClienteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ClienteService {

    @Autowired
	private ClienteRepository repository;

    public List<Cliente> search(Cliente filter){
		ExampleMatcher matcher = ExampleMatcher
		.matching()
		.withIgnoreCase()
		.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		Example example = Example.of(filter, matcher);

		return repository.findAll(example);
	}

	public Cliente getById(Long id){
		return repository
		.findById(id)
		.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}

	@Transactional
	public Cliente create(Cliente cliente){
		return repository.save(cliente);
	}

	@Transactional
	public void update(Long id, Cliente cliente){
		repository.findById(id).map(
			clienteExistente -> {
				cliente.setId(clienteExistente.getId());
				return repository.save(cliente);
			}).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}
	
	@Transactional
	public void delete(Long id){
		repository
			.findById(id)
			.map(
				cliente -> {
					repository.delete(cliente);
					return cliente;
				}
			).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}


}
