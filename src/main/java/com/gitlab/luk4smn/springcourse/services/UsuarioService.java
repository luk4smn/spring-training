package com.gitlab.luk4smn.springcourse.services;

import javax.validation.Valid;

import com.gitlab.luk4smn.springcourse.exceptions.SenhaInvalidaException;
import com.gitlab.luk4smn.springcourse.models.Usuario;
import com.gitlab.luk4smn.springcourse.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UsuarioService implements UserDetailsService{

   
    @Autowired
    PasswordEncoder encoder;
   
    @Autowired
    UsuarioRepository repository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario user = repository
            .findByLogin(username)
            .orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado"));

        String [] roles = user.isAdmin() ? new String[] {"ADMIN", "USER"} : new String[] {"USER"};

        return User
                .builder()
                .username(user.getLogin())
                .password(user.getSenha())
                .roles(roles)
                .build();
    }

    @Transactional
    public Usuario create(@Valid Usuario user) {
        return repository.save(user);
    }


    public UserDetails authenticate(Usuario user){
        UserDetails details = loadUserByUsername(user.getLogin());
        boolean passwordMatch = encoder.matches(user.getSenha(), details.getPassword());

        if(passwordMatch){
            return details;
        }

        throw new SenhaInvalidaException();  
    }
    
}
