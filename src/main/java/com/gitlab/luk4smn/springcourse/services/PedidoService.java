package com.gitlab.luk4smn.springcourse.services;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


import com.gitlab.luk4smn.springcourse.dto.InfoItemPedidoDTO;
import com.gitlab.luk4smn.springcourse.dto.InfoPedidoDTO;
import com.gitlab.luk4smn.springcourse.dto.ItemPedidoDTO;
import com.gitlab.luk4smn.springcourse.dto.PedidoDTO;
import com.gitlab.luk4smn.springcourse.enums.StatusPedido;
import com.gitlab.luk4smn.springcourse.exceptions.PedidoNotFoundException;
import com.gitlab.luk4smn.springcourse.exceptions.RegraDeNegocioException;
import com.gitlab.luk4smn.springcourse.models.Cliente;
import com.gitlab.luk4smn.springcourse.models.ItemPedido;
import com.gitlab.luk4smn.springcourse.models.Pedido;
import com.gitlab.luk4smn.springcourse.models.Produto;
import com.gitlab.luk4smn.springcourse.repositories.ClienteRepository;
import com.gitlab.luk4smn.springcourse.repositories.ItemPedidoRepository;
import com.gitlab.luk4smn.springcourse.repositories.PedidoRepository;
import com.gitlab.luk4smn.springcourse.repositories.ProdutoRepository;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PedidoService {
    
    private final PedidoRepository repository;
    private final ClienteRepository clienteRepository;
    private final ProdutoRepository produtoRepository;
    private final ItemPedidoRepository itemPedidoRepository;

    @Transactional //para garantir integridade ao salvar (ou faz tudo ou em caso de erro faz rollback)
    public Pedido salvar(PedidoDTO dto)
    {
        Pedido pedido = new Pedido();

        Cliente cliente = clienteRepository.findById(dto.getCliente()).orElseThrow(
            () -> new RegraDeNegocioException("Código de cliente inválido")
        );

        pedido.setTotal(dto.getTotal()); 
        pedido.setDataPedido(LocalDate.now());
        pedido.setCliente(cliente);
        pedido.setStatus(StatusPedido.REALIZADO);

        Set<ItemPedido> convertedItens = convertItens(pedido, dto.getItens());

        repository.save(pedido);
        itemPedidoRepository.saveAll(convertedItens);
        pedido.setItens(convertedItens);

        return pedido;
    }

    private Set<ItemPedido> convertItens(Pedido pedido, List<ItemPedidoDTO> itens)
    {
        if(itens.isEmpty()){
            throw new RegraDeNegocioException("Não é possível realizar pedido sem itens.");
        }

        return itens
            .stream()
            .map( dto -> {
                    ItemPedido itemPedido = new ItemPedido();
                    Produto produto = produtoRepository
                    .findById(dto.getProduto())
                    .orElseThrow(
                        () -> new RegraDeNegocioException("Código de produto inválido")
                    );

                    itemPedido.setQuantidade(dto.getQuantidade());
                    itemPedido.setPedido(pedido);
                    itemPedido.setProduto(produto);

                    return itemPedido;
                }
            ).collect(Collectors.toSet());
    }

    public InfoPedidoDTO obterPedido(Long id)
    {
        return repository
                    .findByIdFetchItens(id)
                    .map( p -> converterPedido(p)).orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.NOT_FOUND)
                    );
    }


    private InfoPedidoDTO converterPedido(Pedido pedido)
    {
        return InfoPedidoDTO
                    .builder()
                    .id(pedido.getId())
                    .dataPedido(pedido.getDataPedido().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                    .nomeCliente(pedido.getCliente().getNome())
                    .cpf(pedido.getCliente().getCpf())
                    .total(pedido.getTotal())
                    .status(pedido.getStatus().name())
                    .itens(converterPedidoItens(pedido.getItens()))
                    .build();
    }

    private Set<InfoItemPedidoDTO> converterPedidoItens(Set<ItemPedido> itens)
    {
        if(CollectionUtils.isEmpty(itens)){
            return (Set) Collections.emptyList();
        }

        return itens
            .stream()
            .map( item -> InfoItemPedidoDTO
                        .builder()
                        .descricao(item.getProduto().getDescricao())
                        .preco(item.getProduto().getPreco())
                        .quantidade(item.getQuantidade())
                        .build()
            ).collect(Collectors.toSet());
    }

    @Transactional
    public void atualizaStatus(Long id, StatusPedido statusPedido){
        repository
            .findById(id)
            .map( pedido -> {
                pedido.setStatus(statusPedido);
                return repository.save(pedido);
            })
            .orElseThrow(
                () -> new PedidoNotFoundException("Pedido Não Encontrado")
            );
    }

}
