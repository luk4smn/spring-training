package com.gitlab.luk4smn.springcourse.security.jwt;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.gitlab.luk4smn.springcourse.models.Usuario;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtService {

    @Value("${security.jwt.expiration}")
    private String expiration;

    @Value("${security.jwt.signature-key}")
    private String signatureKey;

    public String gerarToken(Usuario user){
        long expString = Long.valueOf(expiration);
        LocalDateTime dataHoraExpiration = LocalDateTime.now().plusMinutes(expString);
        Instant instant = dataHoraExpiration.atZone(ZoneId.systemDefault()).toInstant();
        Date date = Date.from(instant);

        return Jwts
                .builder()
                .setSubject(user.getLogin())
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, signatureKey)
                .compact();
    }

    private Claims obterClaims(String token) throws ExpiredJwtException{
        return Jwts
                .parser()
                .setSigningKey(signatureKey)
                .parseClaimsJws(token)
                .getBody();
    }

    public String obterUserLogin(String token) throws ExpiredJwtException{
        return (String) obterClaims(token).getSubject();
    }


    public boolean tokenValido(String token){
        try {
            Claims claim = obterClaims(token);
            Date dataExpiracao = claim.getExpiration();
            LocalDateTime dateTime = dataExpiracao.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            
            return !LocalDateTime.now().isAfter(dateTime); 
            
        } catch (Exception e) {
            return false;
        }
    }


    
}
