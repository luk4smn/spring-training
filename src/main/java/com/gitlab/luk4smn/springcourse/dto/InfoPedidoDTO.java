package com.gitlab.luk4smn.springcourse.dto;

import java.math.BigDecimal;
import java.util.Set;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class InfoPedidoDTO {

    private Long id;
    private String nomeCliente;
    private String cpf;
    private String dataPedido;
    private String status;
    private BigDecimal total;
    private Set<InfoItemPedidoDTO> itens;
    
}
