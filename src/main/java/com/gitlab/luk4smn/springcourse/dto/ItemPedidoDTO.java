package com.gitlab.luk4smn.springcourse.dto;

import javax.validation.constraints.NotNull;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ItemPedidoDTO {
    
    @NotNull(message =  "Informe o código do do produto")
    private Long produto;

    @NotNull(message =  "Quantidade é obrigatória")
    private Integer quantidade;
}
