package com.gitlab.luk4smn.springcourse.dto;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class InfoItemPedidoDTO {

    private String descricao;
    private BigDecimal preco;
    private Integer quantidade;
    
}
