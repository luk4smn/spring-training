package com.gitlab.luk4smn.springcourse.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.gitlab.luk4smn.springcourse.validation.NotEmptyList;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PedidoDTO {

    @NotNull(message =  "Informe o código do cliente")
    private Long cliente;

    @NotNull(message =  "Total do pedido é obrigatório")
    private BigDecimal total;

    @NotEmptyList
    private List<ItemPedidoDTO> itens;
    
}
