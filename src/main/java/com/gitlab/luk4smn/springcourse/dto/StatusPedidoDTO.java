package com.gitlab.luk4smn.springcourse.dto;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusPedidoDTO {

    @NotEmpty(message = "Informe o novo status")
    private String newStatus;
}
