package com.gitlab.luk4smn.springcourse.config;

import com.gitlab.luk4smn.springcourse.security.jwt.JwtAuthFilter;
import com.gitlab.luk4smn.springcourse.security.jwt.JwtService;
import com.gitlab.luk4smn.springcourse.services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.OncePerRequestFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{


    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private JwtService jwtService;


    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public OncePerRequestFilter jwtFilter(){
        return new JwtAuthFilter(jwtService, usuarioService);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
       auth
       .userDetailsService(usuarioService)
       .passwordEncoder(passwordEncoder());

        // AUTENTICAÇÃO EM MEMÓRIA
        // auth.inMemoryAuthentication()
        //     .passwordEncoder(passwordEncoder())
        //     .withUser("user")
        //     .password(passwordEncoder().encode("123"))
        //     .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // JWT
        http.csrf().disable()
            .authorizeRequests()
                .antMatchers("/api/clientes/**").hasAnyRole("USER", "ADMIN")
                .antMatchers("/api/pedidos/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.GET, "/api/produtos/**").permitAll()
                .antMatchers(HttpMethod.GET, "/api/base64/**").permitAll()
                .antMatchers(HttpMethod.POST, "/api/users/**").permitAll()
                .anyRequest().authenticated()
            .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class);


        // AUTENTICAÇÃO BASIC
        // http.csrf().disable()
        //     .authorizeRequests()
        //         .antMatchers("/api/clientes/*").hasAnyRole("USER", "ADMIN")
        //         .antMatchers("/api/produtos/*").hasRole("ADMIN")
        //         .antMatchers("/api/pedidos/*").hasAnyRole("USER", "ADMIN")
        //         .antMatchers(HttpMethod.POST, "/api/users/*").permitAll()
        //         .anyRequest().authenticated()
        //     .and()
        //         .httpBasic();
    }   


    //SWAGGER
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }
}
