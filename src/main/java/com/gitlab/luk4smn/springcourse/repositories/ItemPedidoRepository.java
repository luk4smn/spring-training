package com.gitlab.luk4smn.springcourse.repositories;

import com.gitlab.luk4smn.springcourse.models.ItemPedido;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemPedidoRepository extends JpaRepository<ItemPedido, Long> {
    
}
