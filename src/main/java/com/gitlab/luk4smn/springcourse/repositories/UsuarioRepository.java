package com.gitlab.luk4smn.springcourse.repositories;

import java.util.Optional;

import com.gitlab.luk4smn.springcourse.models.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    
    Optional<Usuario> findByLogin(String login);
}
