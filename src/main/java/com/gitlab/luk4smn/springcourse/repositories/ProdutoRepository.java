package com.gitlab.luk4smn.springcourse.repositories;

import com.gitlab.luk4smn.springcourse.models.Produto;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<Produto, Long> {
    
}
