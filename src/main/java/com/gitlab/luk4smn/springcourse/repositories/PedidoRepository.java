package com.gitlab.luk4smn.springcourse.repositories;

import java.util.List;
import java.util.Optional;

import com.gitlab.luk4smn.springcourse.models.Cliente;
import com.gitlab.luk4smn.springcourse.models.Pedido;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    List<Pedido> findByCliente(Cliente cliente);

    @Query("select p from Pedido p left join fetch p.itens where p.id = :id")
    Optional<Pedido> findByIdFetchItens(@Param("id") Long id);
}
