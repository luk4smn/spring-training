package com.gitlab.luk4smn.springcourse.controllers;

import javax.validation.Valid;

import com.gitlab.luk4smn.springcourse.dto.InfoPedidoDTO;
import com.gitlab.luk4smn.springcourse.dto.PedidoDTO;
import com.gitlab.luk4smn.springcourse.dto.StatusPedidoDTO;
import com.gitlab.luk4smn.springcourse.enums.StatusPedido;
import com.gitlab.luk4smn.springcourse.models.Pedido;
import com.gitlab.luk4smn.springcourse.services.PedidoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pedidos")
public class PedidoController {
    
    @Autowired
	private PedidoService service;

    @GetMapping("/{id}")
	private InfoPedidoDTO findById(@PathVariable Long id)
	{
		return service.obterPedido(id);
	}

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Long create(@RequestBody @Valid PedidoDTO dto) 
	{
		Pedido pedido = service.salvar(dto);
		return pedido.getId(); 
	}
	
	@PatchMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void update(@PathVariable Long id, @RequestBody @Valid StatusPedidoDTO dto)
	{
		String novoStatus = dto.getNewStatus();

		service.atualizaStatus(id, StatusPedido.valueOf(novoStatus));
	}


}
