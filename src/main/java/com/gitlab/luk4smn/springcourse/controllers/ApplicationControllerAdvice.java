package com.gitlab.luk4smn.springcourse.controllers;

import java.util.List;
import java.util.stream.Collectors;

import com.gitlab.luk4smn.springcourse.exceptions.ApiErrors;
import com.gitlab.luk4smn.springcourse.exceptions.PedidoNotFoundException;
import com.gitlab.luk4smn.springcourse.exceptions.RegraDeNegocioException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationControllerAdvice {
    
    @ExceptionHandler(RegraDeNegocioException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleRegraDeNegocioException(RegraDeNegocioException ex){

        String message = ex.getMessage();

        return new ApiErrors(message);
    }

    @ExceptionHandler(PedidoNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiErrors handlePedidoNotFoundException(PedidoNotFoundException ex){

        String message = ex.getMessage();

        return new ApiErrors(message);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrors handleMethodNotValidException(MethodArgumentNotValidException ex){

        List<String> errors = ex
                                .getBindingResult()
                                .getAllErrors()
                                .stream()
                                .map(e -> e.getDefaultMessage())
                                .collect(Collectors.toList());

        return new ApiErrors(errors);
    }
}
