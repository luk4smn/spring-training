package com.gitlab.luk4smn.springcourse.controllers;

import java.util.List;

import javax.validation.Valid;

import com.gitlab.luk4smn.springcourse.models.Cliente;
import com.gitlab.luk4smn.springcourse.services.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/clientes")
public class ClienteController {

    @Autowired
	private ClienteService service;

    @GetMapping
	public List<Cliente> find( Cliente filtro) 
	{
		return service.search(filtro);	

       	// return ResponseEntity.ok(service.search(filtro)).getBody();
	}

    @GetMapping("/{id}")
	private Cliente getById(@PathVariable Long id)
	{
		return service.getById(id);

        // Optional<Cliente> cliente = service.getById(id);

		// if(cliente.isPresent()){
		// 	return ResponseEntity.ok(cliente.get());
		// }else{
		// 	return ResponseEntity.notFound().build();
		// }
	}

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Cliente create(@RequestBody @Valid Cliente cliente) 
	{
		return service.create(cliente);

		// Cliente savedCliente = service.create(cliente);
        
        // URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        // .buildAndExpand(savedCliente.getId()).toUri();
        
        // return ResponseEntity.created(location).body(savedCliente);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody @Valid Cliente cliente)
	{
		service.update(id, cliente);
		
		// if(service.update(id, cliente) != null){
		// 	return ResponseEntity.ok().build();
		// } 

		// return ResponseEntity.notFound().build();
	}

    @DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	private void deleteById(@PathVariable Long id)
	{
        
		service.delete(id);
		
		// Optional<Cliente> cliente = service.getById(id);

		// if(cliente.isPresent()){
        //     service.delete(cliente.get().getId());
		// 	return ResponseEntity.ok().build();
		// }else{
		// 	return ResponseEntity.notFound().build();
		// }
	}
	
    
}
