package com.gitlab.luk4smn.springcourse.controllers;

import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sun.misc.BASE64Decoder;
import java.io.File;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;

@RestController
@RequestMapping("/api/base64")
public class Base64Controller {

    @SneakyThrows
    @GetMapping("/decode")
    private void decodeToFile()
    {
        //String que vem da request
        String base64 = "H4sIAAAAAAACCuyUuw6CQBBFe75iM73MzrIgEB6dH2CkNgQRSIQlLIif78ZQ2FhoZbG3m5vJSU5zk%2FzR39i9nnSnhhTI5cDqoVKXbmhSKE6HXQh55iSFVseFmd9Bp9DO8xgjruvq6m6u%2B3LUrpoa1FVrDo1bidyNIHPYFsNYyqlTrBqvKXARUBiF0ucE7DKfF61MSUiEghNn5MXkx0IAfiSEexkZhpTvBPEVQXBOgRf44leCtbAW1sJa%2FI1Fgq%2BpzpwnAAAA%2F%2F8DAEoWM%2FnaBQAA";

        //path para salvar arquivo + nome do arquivo + extensão
        String outputPath = System.getProperty("user.home") + File.separator + "Downloads" + File.separator + "relatorio.xml.gz";

        // Decodifica a string Base64 URL
        String carg = URLDecoder.decode(base64, "ISO-8859-1");
        carg = carg.replace("\n","");

        //BASE64Decoder decoder = new BASE64Decoder();
        //byte[] decodedBytes = decoder.decodeBuffer(carg);
        byte[] decodedBytes =  Base64.getDecoder().decode(carg);

        // Cria um arquivo temporário com os bytes decodificados
        Path tempFile = Files.createTempFile("decoded", ".gz");
        try (OutputStream os = Files.newOutputStream(tempFile)) {
            os.write(decodedBytes);
        }

        // Move o arquivo temporário para o caminho de saída especificado
        Files.move(tempFile, Paths.get(outputPath), StandardCopyOption.REPLACE_EXISTING);
    }
}
