package com.gitlab.luk4smn.springcourse.controllers;

import javax.validation.Valid;

import com.gitlab.luk4smn.springcourse.dto.CredenciaisDTO;
import com.gitlab.luk4smn.springcourse.dto.TokenDTO;
import com.gitlab.luk4smn.springcourse.exceptions.SenhaInvalidaException;
import com.gitlab.luk4smn.springcourse.models.Usuario;
import com.gitlab.luk4smn.springcourse.security.jwt.JwtService;
import com.gitlab.luk4smn.springcourse.services.UsuarioService;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UsuarioController {
    
    private final UsuarioService service;
	private final JwtService jwtService;
    private final PasswordEncoder encoder;

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario create(@RequestBody @Valid Usuario user) 
	{
        String encodedPassword = encoder.encode(user.getSenha());
        user.setSenha(encodedPassword);

		return service.create(user);
	}


	@PostMapping("/auth")
	public TokenDTO authenticate(@RequestBody CredenciaisDTO dto)
	{
		try {
			Usuario user = Usuario
							.builder()
							.login(dto.getLogin())
							.senha(dto.getSenha())
							.build();

			UserDetails authenticatedUser =  service.authenticate(user);
				
			String token = jwtService.gerarToken(user);
			
			return new TokenDTO(authenticatedUser.getUsername(), token);

		} catch (UsernameNotFoundException | SenhaInvalidaException e) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
		}
	}
}
