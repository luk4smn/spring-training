package com.gitlab.luk4smn.springcourse.controllers;

import java.util.List;

import javax.validation.Valid;

import com.gitlab.luk4smn.springcourse.models.Produto;
import com.gitlab.luk4smn.springcourse.services.ProdutoService;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/produtos")
public class ProdutoController {

    @Autowired
	private ProdutoService service;

    @GetMapping
	public List<Produto> find( Produto filtro) 
	{
		return service.search(filtro);	
	}

    @GetMapping("/{id}")
	private Produto findById(@PathVariable Long id)
	{
		return service.getById(id);
	}

    @PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Produto create(@RequestBody @Valid Produto produto) 
	{
		return service.create(produto);
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void update(@PathVariable Long id, @RequestBody @Valid Produto produto)
	{
		service.update(id, produto);
	}

    @DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	private void deleteById(@PathVariable Long id)
	{
		service.delete(id);
	}
}
