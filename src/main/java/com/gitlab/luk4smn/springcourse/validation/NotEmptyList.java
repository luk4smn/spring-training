package com.gitlab.luk4smn.springcourse.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.gitlab.luk4smn.springcourse.validation.constraint.NotEmptyListValidator;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = NotEmptyListValidator.class)
public @interface NotEmptyList {
    
    String message() default "A lista não pode estar vazia";

    Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

}
