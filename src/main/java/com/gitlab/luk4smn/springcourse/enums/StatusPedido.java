package com.gitlab.luk4smn.springcourse.enums;

public enum StatusPedido {
    REALIZADO,
    CANCELADO;
}
