package com.gitlab.luk4smn.springcourse.exceptions;

public class RegraDeNegocioException extends RuntimeException {

    public RegraDeNegocioException(String message) {
        super(message);
    }
    
}
