package com.gitlab.luk4smn.springcourse.exceptions;

public class SenhaInvalidaException extends RuntimeException {

    public SenhaInvalidaException() {
        super("Senha Inválida");
    }
    
}
