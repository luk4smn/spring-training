package com.gitlab.luk4smn.springcourse.exceptions;

public class PedidoNotFoundException extends RuntimeException {

    public PedidoNotFoundException(String message) {
        super(message);
    }
    
}
